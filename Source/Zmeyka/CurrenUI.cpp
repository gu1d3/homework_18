// Fill out your copyright notice in the Description page of Project Settings.


#include "CurrenUI.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SnakeBase.h"

void UCurrenUI::NativeConstruct()
{
	Super::NativeConstruct();

	ShowCurrentScore(0);
	ShowCurrentSpeed(0.f);


}


void UCurrenUI::ShowCurrentScore(int Score)
{

	if (Score)
	{
		CurrentScore->SetText(FText::AsNumber(Score));
	}

}

void UCurrenUI::ShowCurrentSpeed(float Speed)
{

	if (Speed)
	{
		CurrentSpeed->SetText(FText::AsNumber(Speed));
	}
	
}

void UCurrenUI::ShowCurrentDeathTimer(float TimerRemaining)
{
	CurrentDeathTimer->SetText(FText::AsNumber(TimerRemaining));
}
