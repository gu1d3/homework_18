// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "SnakeBase.h"
#include "CurrenUI.generated.h"


/**
 * 
 */
UCLASS()
class ZMEYKA_API UCurrenUI : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION()
	void ShowCurrentScore(int Score);

	UFUNCTION()
	void ShowCurrentSpeed(float Speed);

	UFUNCTION()
	void ShowCurrentDeathTimer(float TimerRemaining);

protected:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* CurrentScore;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* CurrentSpeed;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* CurrentDeathTimer;

	void NativeConstruct() override;


};
