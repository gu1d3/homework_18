// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SpawnBox.h"
#include "CurrenUI.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &AFood::DestroyFood, 5.f, false);
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			//Snake->OnEatenFood.Broadcast(Snake->SnakeElements.Num());

			//Snake->OnEatenFood.Broadcast(Snake->MovementSpeed);

			Snake->UpdateRecordScore();

			//Snake->CompareSave();

			Snake->ResetTimer();

			Destroy();
			
		}
	}
}

void AFood::DestroyFood()
{
	Destroy();
}


