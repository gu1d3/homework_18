// Fill out your copyright notice in the Description page of Project Settings.

#include "GameOverWidget.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SnakeBase.h"
#include "ZmeykaPController.h"
#include "Blueprint/WidgetBlueprintLibrary.h"




void UGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();

	RestartButton->OnClicked.AddUniqueDynamic(this, &UGameOverWidget::OnRestartButtonClicked);
	MenuButton->OnClicked.AddUniqueDynamic(this, &UGameOverWidget::OnMainMenuClicked);

	ShowPCursor();
	
}

void UGameOverWidget::ShowFinalScore(int Score)
{
	if (Score)
	{
		FinalScore->SetText(FText::AsNumber(Score));
	}
}

void UGameOverWidget::ShowCurrentRecordScore(int RecordScore)
{

	if (RecordScore)
	{
		CurrentRecordScore->SetText(FText::AsNumber(RecordScore));
	}

}

void UGameOverWidget::OnRestartButtonClicked()
{
	RestartGame();
	RemovePCursor();
}

void UGameOverWidget::OnMainMenuClicked()
{
	GoToMenu();
	RemovePCursor();
}

void UGameOverWidget::RestartGame()
{
	UGameplayStatics::OpenLevel(this, "Main");
}

void UGameOverWidget::GoToMenu()
{
	UGameplayStatics::OpenLevel(this,"MainMenu");
}

void UGameOverWidget::ShowPCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{

		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;

		UWidgetBlueprintLibrary::SetInputMode_GameAndUI(PC, this);

	}

}

void UGameOverWidget::RemovePCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{

		PC->bShowMouseCursor = false;
		PC->bEnableClickEvents = false;
		PC->bEnableMouseOverEvents = false;

		UWidgetBlueprintLibrary::SetInputMode_GameOnly(PC);

	}

}

