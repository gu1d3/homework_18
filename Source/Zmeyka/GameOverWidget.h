// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "GameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION()
	void ShowFinalScore(int Score);

	UFUNCTION()
	void ShowCurrentRecordScore(int RecordScore);

	UFUNCTION()
	void OnRestartButtonClicked();

	UFUNCTION()
	void OnMainMenuClicked();

	UFUNCTION()
	void RestartGame();

	UFUNCTION()
	void GoToMenu();

	UFUNCTION()
	void ShowPCursor();

	UFUNCTION()
	void RemovePCursor();

protected:

	UPROPERTY(meta = (BindWidget))
	UTextBlock* FinalScore;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* CurrentRecordScore;

	UPROPERTY(meta = (BindWidget))
	UButton* RestartButton;

	UPROPERTY(meta = (BindWidget))
	UButton* MenuButton;

	void NativeConstruct() override;

};
