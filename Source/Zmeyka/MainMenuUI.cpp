// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuUI.h"
#include "RunTime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Blueprint/WidgetBlueprintLibrary.h"



void UMainMenuUI::NativeConstruct()
{

	Super::NativeConstruct();

	StartButton->OnClicked.AddUniqueDynamic(this, &UMainMenuUI::OnStartButtonClicked);

	ShowPCursor();

}

void UMainMenuUI::OnStartButtonClicked()
{

	StartGame();
	RemovePCursor();

}

void UMainMenuUI::StartGame()
{
	UGameplayStatics::OpenLevel(this, "Main");
}

void UMainMenuUI::ShowPCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{
		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;

		UWidgetBlueprintLibrary::SetInputMode_GameAndUI(PC, this);

	}

}

void UMainMenuUI::RemovePCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{
		PC->bShowMouseCursor = false;
		PC->bEnableClickEvents = false;
		PC->bEnableMouseOverEvents = false;

		UWidgetBlueprintLibrary::SetInputMode_GameOnly(PC);

	}

}
