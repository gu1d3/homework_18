// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "MainMenuUI.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API UMainMenuUI : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	
	UFUNCTION()
	void OnStartButtonClicked();

	UFUNCTION()
	void StartGame();

	UFUNCTION()
	void ShowPCursor();

	UFUNCTION()
	void RemovePCursor();

	UPROPERTY(meta = (BindWidget))
	UTextBlock* MainMenuUI;

	UPROPERTY(meta = (BindWidget))
	UButton* StartButton;


	void NativeConstruct() override;

};
