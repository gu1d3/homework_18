// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"

#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

#include "SnakeBase.h"

#include "GameOverWidget.h"
#include "GameOver.h"
#include "CurrenUI.h"
#include "VictoryMenu.h"





// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	ShowCurrentUI();
	CreateSnakeActor();

	//WidgetToSpawn->OnDead.AddDynamic(this, &APlayerPawnBase::ShowWidget);

	//FTimerHandle Handle;
	//GetWorld()->GetTimerManager().SetTimer(Handle, this, &APlayerPawnBase::DestroyPPawnBase, 25.f, false);


}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);


}

void APlayerPawnBase::CreateSnakeActor()
{

	FTransform LocTrans = FTransform::Identity;
	LocTrans.SetLocation(FVector(0, 0, 50));
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, LocTrans);

	if (SnakeActor)
	{

		ChangedScore(SnakeActor->ElementarySize);
		ChangedSpeed(SnakeActor->MovementSpeed);

		SnakeActor->OnDead.AddDynamic(this, &APlayerPawnBase::ShowWidget);

		SnakeActor->OnWin.AddDynamic(this, &APlayerPawnBase::ShowVictoryWidget);

		SnakeActor->OnEatenFood.AddDynamic(this,&APlayerPawnBase::ChangedScore);

		SnakeActor->OnEatenSpeedFood.AddDynamic(this, &APlayerPawnBase::ChangedSpeed);

		SnakeActor->OnDeadScore.AddDynamic(this, &APlayerPawnBase::SendFinalScore);

		SnakeActor->OnDeadRScore.AddDynamic(this, &APlayerPawnBase::SendFinalRScore);

		SnakeActor->OnWinScore.AddDynamic(this, &APlayerPawnBase::SendVictoryScore);

		SnakeActor->OnWinRScore.AddDynamic(this, &APlayerPawnBase::SendVictoryRScore);

		SnakeActor->DeathTimer.AddDynamic(this, &APlayerPawnBase::ShowCurrentTimer);

	}

}
 
void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor -> LastMoveDirection !=EmovementDirection::DOWN && !SnakeActor->Moving)
		{
			SnakeActor->LastMoveDirection = EmovementDirection::UP;
			SnakeActor->Moving = true;
		}
		else if (value < 0 && SnakeActor -> LastMoveDirection != EmovementDirection::UP && !SnakeActor->Moving)
		{
			SnakeActor->LastMoveDirection = EmovementDirection::DOWN;
			SnakeActor->Moving = true;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor -> LastMoveDirection != EmovementDirection::LEFT && !SnakeActor->Moving)
		{
			SnakeActor->LastMoveDirection = EmovementDirection::RIGHT;
			SnakeActor->Moving = true;
		}
		else if (value < 0 && SnakeActor -> LastMoveDirection != EmovementDirection::RIGHT && !SnakeActor->Moving)
		{
			SnakeActor->LastMoveDirection = EmovementDirection::LEFT;
			SnakeActor->Moving = true;
		}
	}
}

void APlayerPawnBase::ShowWidget()
{
	if (WidgetToBeSpawned)
	{
		Widget = Cast<UGameOverWidget>(CreateWidget(GetWorld(), WidgetToBeSpawned));

		if (Widget)
		{

			Widget->AddToViewport();

		}

	}
}

void APlayerPawnBase::ChangedScore(int IncreaseScore)
{

	if (CurrentUI)
	{
		CurrentUI->ShowCurrentScore(IncreaseScore);
	}
	
}

void APlayerPawnBase::ChangedSpeed(float IncreaseSpeed)
{

	if (CurrentUI)
	{
		CurrentUI->ShowCurrentSpeed(IncreaseSpeed);
	}
	
}

void APlayerPawnBase::ShowCurrentUI()
{
	if (CurrentUIToBeSpawned)
	{
		CurrentUI = Cast<UCurrenUI>(CreateWidget(GetWorld(), CurrentUIToBeSpawned));

		if (CurrentUI)
		{
			CurrentUI->AddToViewport();
		}

	}
}

void APlayerPawnBase::ShowVictoryWidget()
{

	if (VictoryWidgetToBeSpawned)
	{

		VictoryMenu = Cast<UVictoryMenu>(CreateWidget(GetWorld(), VictoryWidgetToBeSpawned));

		if (VictoryMenu)
		{
			VictoryMenu->AddToViewport();
		}

	}

}

void APlayerPawnBase::SendFinalScore(int OnDeadScore)
{
	if (Widget)
	{
		Widget->ShowFinalScore(OnDeadScore);
	}
}

void APlayerPawnBase::SendFinalRScore(int OnDeadRScore)
{
	if (Widget)
	{
		Widget->ShowCurrentRecordScore(OnDeadRScore);
	}
}

void APlayerPawnBase::SendVictoryScore(int OnWinScore)
{
	if (VictoryMenu)
	{
		VictoryMenu->ShowFinalScore(OnWinScore);
	}
}

void APlayerPawnBase::SendVictoryRScore(int OnWinRScore)
{
	if (VictoryMenu)
	{
		VictoryMenu->ShowFinalRScore(OnWinRScore);
	}
}

void APlayerPawnBase::ShowCurrentTimer(float DeathTimer)
{
	if (CurrentUI)
	{
		CurrentUI->ShowCurrentDeathTimer(DeathTimer);
	}
}

void APlayerPawnBase::DestroyPPawnBase()
{
	Destroy();
}

void APlayerPawnBase::RemoveCursor()
{
	
	//APlayerController* PC = GetWorld()->GetFirstPlayerController();

}

