// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class ZMEYKA_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase>SnakeActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUserWidget> WidgetToBeSpawned;

	UPROPERTY(VisibleInstanceOnly,Category="Runtime")
	class UGameOverWidget* Widget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUserWidget> CurrentUIToBeSpawned;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
	class UCurrenUI* CurrentUI;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TSubclassOf<class UUserWidget> VictoryWidgetToBeSpawned;

	UPROPERTY(VisibleInstanceOnly,Category = "Runtime")
	class UVictoryMenu* VictoryMenu;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	class ASnakeBase* WidgetToSpawn;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION(BlueprintCallable)
	void ShowWidget();

	UFUNCTION()
	void ShowCurrentUI();

	UFUNCTION(BlueprintCallable)
	void ShowVictoryWidget();

	UFUNCTION()
	void ChangedScore(int IncreaseScore);

	UFUNCTION()
	void ChangedSpeed(float IncreaseSpeed);

	UFUNCTION()
	void SendFinalScore(int OnDeadScore);

	UFUNCTION()
	void SendFinalRScore(int OnDeadRScore);

	UFUNCTION()
	void SendVictoryScore(int OnWinScore);

	UFUNCTION()
	void SendVictoryRScore(int OnWinRScore);

	UFUNCTION()
	void ShowCurrentTimer(float DeathTimer);
	
	UFUNCTION()
	void DestroyPPawnBase();

	UFUNCTION()
	void RemoveCursor();


};
