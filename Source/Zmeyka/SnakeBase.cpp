// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "ZmeykaSaveGame.h"
#include <math.h>
#include "ZmeykaGameInstance.h"



// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	ElementarySize = 4;
	LastMoveDirection = EmovementDirection::DOWN;
	Moving = false;
	DeathTime = 15;
	VictorySize = 20;
	RecordSize = 0;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	GameInst = Cast<UZmeykaGameInstance>(GetGameInstance());

	if (GameInst)
	{
		GameInst->LoadGame();
		RecordSize = GameInst->MaxRecord;
	}
	

	SetActorTickInterval(MovementSpeed);

	if (ElementarySize)
	{
		AddSnakeElement(ElementarySize);
	}

	GetWorld()->GetTimerManager().SetTimer(Timer, this, &ASnakeBase::DestroySnake, DeathTime, false);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	Move();

	DeathTimer.Broadcast(round(GetWorld()->GetTimerManager().GetTimerRemaining(Timer)*100)/100);
	
	if (SnakeElements.Num())
	{
		if (SnakeElements.Num() == VictorySize)
		{
			OnVictory();
		}
	}
}

int ASnakeBase::SaveRecordScore()
{
	return RecordSize;
}

/*void ASnakeBase::CompareSave()
{

	auto GameInst = Cast<UZmeykaGameInstance>(GetGameInstance());

	UZmeykaSaveGame* dataToLoad = Cast<UZmeykaSaveGame>(UGameplayStatics::LoadGameFromSlot("Slot1", 0));

	if (dataToLoad != nullptr)
	{
		auto CompRecordSize = dataToLoad->RecordSize;
		if (RecordSize > CompRecordSize)
		{
			GameInst->SaveGame();
			UE_LOG(LogTemp, Warning, TEXT("Saving..."));
		}
	}
	else if (!UGameplayStatics::DoesSaveGameExist("Slot1", 0))
	{
		GameInst->CreateSaveFile();

		GameInst->SaveGame();

		UE_LOG(LogTemp, Warning, TEXT("Saving..."));

	}

}*///nothing

void ASnakeBase::AddSnakeElement(int ElementsNum)
{

	for (int i = 0; i < ElementsNum; ++i)
	{	

		if (SnakeElements.Num() == 0)
		{
			PrevLocY = 0;
			PrevLocX = 0;
		}

		else
		{
			PrevLocY = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y;
			PrevLocX = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X;
		}

		//FVector NewLocation(SnakeElements.Num() * ElementSize, PrevLocY, 0); ������ �  ���� ����������, ��� ��� ���� ���� �����
		FVector NewLocation(PrevLocX, PrevLocY, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex==0)
		{
			NewSnakeElem->SetFirstElementType();
		}

		OnEatenFood.Broadcast(SnakeElements.Num());
		OnEatenSpeedFood.Broadcast(MovementSpeed);

	}

}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EmovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EmovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EmovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EmovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;

	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLovation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLovation);

	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	Moving = false;

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);

		}
	} 
}

void ASnakeBase::SpeedUp(float DeltaSpeed)
{
	MovementSpeed *= DeltaSpeed;
	SetActorTickInterval(MovementSpeed);

	OnEatenSpeedFood.Broadcast(MovementSpeed);

}

void ASnakeBase::DeleteSnakeElement()
{
	auto LastElem = SnakeElements[SnakeElements.Num() - 1];
	GetWorld()->DestroyActor(LastElem);
}

void ASnakeBase::OnDeath()
{
	if (GameInst)
	{
		GameInst->SaveGame(RecordSize);
	}
	OnDead.Broadcast();
}

void ASnakeBase::OnDeathScore()
{
	OnDeadScore.Broadcast(SnakeElements.Num());
}

void ASnakeBase::OnDeathRScore()
{
	OnDeadRScore.Broadcast(RecordSize);
}

void ASnakeBase::UpdateScore()
{
	OnEatenFood.Broadcast(SnakeElements.Num());
}

void ASnakeBase::UpdateSpeed()
{
	OnEatenSpeedFood.Broadcast(MovementSpeed);
}

void ASnakeBase::DestroySnake()
{
	UpdateRecordScore();
	OnDeath();
	OnDeathScore();
	OnDeathRScore();
	Destroy();
}

void ASnakeBase::UpdateRecordScore()
{
	RecordSize = std::max(RecordSize, SnakeElements.Num());
	//UE_LOG(LogTemp, Warning,TEXT("RecordSizeUpated"));
}	

void ASnakeBase::OnVictory()
{
	if (GameInst)
	{
		GameInst->SaveGame(RecordSize);
	}
	OnWin.Broadcast();
	OnWinScore.Broadcast(SnakeElements.Num());
	OnWinRScore.Broadcast(RecordSize);
	Destroy();
}

void ASnakeBase::ShowTimer()
{
	DeathTimer.Broadcast(GetWorld()->GetTimerManager().GetTimerRemaining(Timer));
}

void ASnakeBase::ResetTimer()
{
	GetWorld()->GetTimerManager().SetTimer(Timer, this, &ASnakeBase::DestroySnake, DeathTime, false);
}
