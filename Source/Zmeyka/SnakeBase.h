// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZmeykaGameInstance.h"
#include "SnakeBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnVictory);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEatenFood, int, IncreaseScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEatenSpeedFood, float, IncreaseSpeed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeathScore, int, OnDeadScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnVictoryScore, int, OnWinScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeathRScore, int, OnDeadRScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnVictoryRScore, int, OnWinRScore);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCurrentDeathTimer, float, DeathTimer);

class ASnakeElementBase;

UENUM()
enum class EmovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class ZMEYKA_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditAnywhere)
	float ElementSize;

	UPROPERTY(EditAnywhere)
	float MovementSpeed;

	UPROPERTY(EditAnywhere)
	float PrevLocY;
	float PrevLocX;

	UPROPERTY(EditAnywhere)
	int ElementarySize;

	UPROPERTY(EditAnywhere)
	int RecordSize;

	UPROPERTY(EditAnywhere)
	int VictorySize;

	UPROPERTY()
	FTimerHandle Timer;	

	UPROPERTY(EditAnywhere)
	int DeathTime;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EmovementDirection	LastMoveDirection;

	UPROPERTY()
	bool Moving;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDead;

	UPROPERTY(BlueprintAssignable)
	FOnVictory OnWin;

	UPROPERTY(BlueprintAssignable)
	FOnEatenFood OnEatenFood;

	UPROPERTY(BlueprintAssignable)
	FOnEatenSpeedFood OnEatenSpeedFood;

	UPROPERTY(BlueprintAssignable)
	FOnDeathScore OnDeadScore;

	UPROPERTY(BlueprintAssignable)
	FOnVictoryScore OnWinScore;

	UPROPERTY(BlueprintAssignable)
	FOnDeathRScore OnDeadRScore;

	UPROPERTY(BlueprintAssignable)
	FOnVictoryRScore OnWinRScore;

	UPROPERTY(BlueprintAssignable)
	FCurrentDeathTimer DeathTimer;

	UPROPERTY()
	UZmeykaGameInstance* GameInst = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	int SaveRecordScore();

	//UFUNCTION()
	//void CompareSave();

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
	void SpeedUp(float DeltaSpeed);

	UFUNCTION(BlueprintCallable)
	void DeleteSnakeElement();

	UFUNCTION()
	void OnDeath();

	UFUNCTION()
	void OnDeathScore();

	UFUNCTION()
	void OnDeathRScore();

	UFUNCTION()
	void UpdateScore();

	UFUNCTION()
	void UpdateSpeed();

	UFUNCTION()
	void DestroySnake();

	UFUNCTION()
	void UpdateRecordScore();

	UFUNCTION()
	void OnVictory();

	UFUNCTION()
	void ShowTimer();

	UFUNCTION()
	void ResetTimer();

};
