// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "GameOverWidget.h"
#include "GameOver.h"
#include "PlayerPawnBase.h"
#include "ZmeykaPController.h"


// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

	//FTimerHandle Handle;
	//GetWorld()->GetTimerManager().SetTimer(Handle, this, &ASnakeElementBase::Interact, 25.f, false);


}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//X
	if (GetActorLocation().X > 620)
	{
		FVector NewLocation(-620, GetActorLocation().Y, GetActorLocation().Z);
		FTransform NewTransform(NewLocation);
		SetActorLocation(NewLocation);
	}
	else if (GetActorLocation().X < -620)
	{
		FVector NewLocation(620, GetActorLocation().Y, GetActorLocation().Z);
		FTransform NewTransform(NewLocation);
		SetActorLocation(NewLocation);
	}

	//Y
	if (GetActorLocation().Y > 1260)
	{
		FVector NewLocation(GetActorLocation().X, -1260, GetActorLocation().Z);
		FTransform NewTransform(NewLocation);
		SetActorLocation(NewLocation);
	}
	else if (GetActorLocation().Y < -1260)
	{
		FVector NewLocation(GetActorLocation().X, 1260, GetActorLocation().Z);
		FTransform NewTransform(NewLocation);
		SetActorLocation(NewLocation);
	}
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(interactor);
//	auto GameOver = Cast<APlayerPawnBase>(interactor);
	if (IsValid(Snake))
	{
		
		Snake->DestroySnake();

	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}
