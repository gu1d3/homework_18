// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnBox.h"
#include "Food.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpawnBox::ASpawnBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnBox"));
	RootComponent = SpawnBox;

}

// Called when the game starts or when spawned
void ASpawnBox::BeginPlay()
{
	Super::BeginPlay();

	auto SpawnedClass = Cast<AActor>(ActorClassToBeSpawned[UKismetMathLibrary::RandomIntegerInRange(0, ActorClassToBeSpawned.Num() - 1)]);
	SpawnActor(SpawnedClass);
	
	//auto BarrierSpawnedClass = Cast<AActor>(BarrierToBeSpawned);
	//for (int i = 0; i < 5; ++i)
	//{
	//	BarrierSpawner(BarrierSpawnedClass);
	//}
	
	
	
}

// Called every frame
void ASpawnBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//void ASpawnBox::SpawnActor(AActor* Spawned)
//{
//	FVector SpawnLocation = SpawnBox->Bounds.Origin;
//
//	SpawnLocation.X += -SpawnBox->Bounds.BoxExtent.X + 2 * SpawnBox->Bounds.BoxExtent.X * FMath::FRand();
//	SpawnLocation.Y += -SpawnBox->Bounds.BoxExtent.Y + 2 * SpawnBox->Bounds.BoxExtent.Y * FMath::FRand();
//	SpawnLocation.Z = 0;
//	
//	Spawned = GetWorld()->SpawnActor(ActorClassToBeSpawned[UKismetMathLibrary::RandomIntegerInRange(0, ActorClassToBeSpawned.Num() - 1)], &SpawnLocation);
//
//	if (Spawned)
//	{
//		Spawned->OnDestroyed.AddDynamic(this, &ASpawnBox::SpawnActor);
//	}
//}

int CustomRandom()
{
	return (FMath::RandRange(0, 1) == 0) ? -1 : 1;

}

void ASpawnBox::SpawnActor(AActor* Spawned)
{
	FVector SpawnLocation = GetActorLocation();

	int x = 0;
	int y = 0;

	x = UKismetMathLibrary::RandomIntegerInRange(-8, 8);
	x *= 65;

	y = UKismetMathLibrary::RandomIntegerInRange(-4, 4);
	y *= 65;

	SpawnLocation.X += x * CustomRandom();
	SpawnLocation.Y += y * CustomRandom();
	SpawnLocation.Z = 0;

	Spawned = GetWorld()->SpawnActor(ActorClassToBeSpawned[UKismetMathLibrary::RandomIntegerInRange(0, ActorClassToBeSpawned.Num() - 1)], &SpawnLocation);

	if (Spawned)
	{
		Spawned->OnDestroyed.AddDynamic(this, &ASpawnBox::SpawnActor);
	}

}


void ASpawnBox::BarrierSpawner(AActor* BarrierSpawned)
{
	FVector SpawnLocation = SpawnBox->Bounds.Origin;

	SpawnLocation.X += -SpawnBox->Bounds.BoxExtent.X + 2 * SpawnBox->Bounds.BoxExtent.X * FMath::FRand();
	SpawnLocation.Y += -SpawnBox->Bounds.BoxExtent.Y + 2 * SpawnBox->Bounds.BoxExtent.Y * FMath::FRand();
	SpawnLocation.Z = 0;

	BarrierSpawned = GetWorld()->SpawnActor(BarrierToBeSpawned, &SpawnLocation);
	if (BarrierSpawned)
	{
		BarrierSpawned->OnDestroyed.AddDynamic(this, &ASpawnBox::BarrierSpawner);
	}
}
