// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpFood.h"
#include "SnakeBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpeedUpFood::ASpeedUpFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedUpFood::BeginPlay()
{
	Super::BeginPlay();
	DeltaSpeed = UKismetMathLibrary::RandomFloatInRange(0.75, 1.25);

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &ASpeedUpFood::DestroySpeedUpFood, 5.f, false);

	
}

// Called every frame
void ASpeedUpFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedUpFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SpeedUp(DeltaSpeed);
			Snake->ResetTimer();
			Destroy();
			
		}
	}
}

void ASpeedUpFood::DestroySpeedUpFood()
{
	Destroy();
}


