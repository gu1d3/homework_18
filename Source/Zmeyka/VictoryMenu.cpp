// Fill out your copyright notice in the Description page of Project Settings.


#include "VictoryMenu.h"
#include "Runtime/Engine/Classes/kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SnakeBase.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void UVictoryMenu::NativeConstruct()
{

	Super::NativeConstruct();

	RestartButton->OnClicked.AddUniqueDynamic(this, &UVictoryMenu::OnRestartButtonClicked);
	MenuButton->OnClicked.AddUniqueDynamic(this, &UVictoryMenu::OnMainMenuClicked);

	ShowPCursor();

}

void UVictoryMenu::ShowFinalScore(int Score)
{

	if (Score)
	{
		FinalScore->SetText(FText::AsNumber(Score));
	}

}

void UVictoryMenu::ShowFinalRScore(int RScore)
{

	if (RScore)
	{
		FinalRScore->SetText(FText::AsNumber(RScore));
	}

}

void UVictoryMenu::OnRestartButtonClicked()
{
	RestartGame();
	RemovePCursor();
}

void UVictoryMenu::OnMainMenuClicked()
{
	GoToMenu();
	RemovePCursor();
}

void UVictoryMenu::RestartGame()
{
	UGameplayStatics::OpenLevel(this, "Main");
}

void UVictoryMenu::GoToMenu()
{
	UGameplayStatics::OpenLevel(this, "MainMenu");
}

void UVictoryMenu::ShowPCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{

		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;

		UWidgetBlueprintLibrary::SetInputMode_GameAndUI(PC, this);

	}

}

void UVictoryMenu::RemovePCursor()
{

	APlayerController* PC = GetWorld()->GetFirstPlayerController();

	if (PC)
	{

		PC->bShowMouseCursor = false;
		PC->bEnableClickEvents = false;
		PC->bEnableMouseOverEvents = false;

		UWidgetBlueprintLibrary::SetInputMode_GameOnly(PC);

	}

}
