// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "SnakeBase.h"
#include "VictoryMenu.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API UVictoryMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION()
	void ShowFinalScore(int Score);

	UFUNCTION()
	void ShowFinalRScore(int RScore);

	UFUNCTION()
	void OnRestartButtonClicked();

	UFUNCTION()
	void OnMainMenuClicked();

	UFUNCTION()
	void RestartGame();

	UFUNCTION()
	void GoToMenu();

	UFUNCTION()
	void ShowPCursor();

	UFUNCTION()
	void RemovePCursor();

protected:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* FinalScore;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* FinalRScore;

	UPROPERTY(meta = (BindWidget))
	UButton* RestartButton;

	UPROPERTY(meta = (BindWidget))
	UButton* MenuButton;

	void NativeConstruct() override;

};
