// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeykaGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "ZmeykaSaveGame.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

UZmeykaGameInstance::UZmeykaGameInstance()
{
	SaveGameSlotName = "Slot1";
}

void UZmeykaGameInstance::Init()
{

	Super::Init();

	if (!dataToSave)
	{
		dataToSave = Cast<UZmeykaSaveGame>(UGameplayStatics::CreateSaveGameObject(UZmeykaSaveGame::StaticClass()));
		UGameplayStatics::SaveGameToSlot(dataToSave, SaveGameSlotName, 0);
	}

} 

void UZmeykaGameInstance::LoadGame()
{

	if (dataToSave)
	{

		UE_LOG(LogTemp, Warning, TEXT("Load file..."));
		dataToSave = Cast<UZmeykaSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveGameSlotName, 0));
		MaxRecord = dataToSave->RecordSize;
		UE_LOG(LogTemp, Warning, TEXT("Result = %i"), MaxRecord);

	}

}

void UZmeykaGameInstance::SaveGame(int Value)
{

	if (UGameplayStatics::DoesSaveGameExist(SaveGameSlotName, 0))
	{

		UE_LOG(LogTemp, Warning, TEXT("Save file..."));
		if (dataToSave)
		{

			if (MaxRecord > Value) return;
			UE_LOG(LogTemp, Warning, TEXT("Saving NEW RESULT..."));
			dataToSave->RecordSize = Value;
			UGameplayStatics::SaveGameToSlot(dataToSave, SaveGameSlotName, 0);

		}

	}

}
