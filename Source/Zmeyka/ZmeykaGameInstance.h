// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintPlatformLibrary.h"
#include "Engine/GameInstance.h"
#include "ZmeykaSaveGame.h"
#include "ZmeykaGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API UZmeykaGameInstance : public UPlatformGameInstance
{
	GENERATED_BODY()
	
public:

	UZmeykaGameInstance();

	virtual void Init() override;

	UPROPERTY(BlueprintReadOnly)
	FString SaveGameSlotName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector Size;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Padding;

	UPROPERTY()
	UZmeykaSaveGame* dataToSave = nullptr;

	UPROPERTY(VisibleAnywhere)
	int MaxRecord = 0;

	UFUNCTION(BlueprintCallable)
	void LoadGame();

	UFUNCTION(BlueprintCallable)
	void SaveGame(int Value);

};
