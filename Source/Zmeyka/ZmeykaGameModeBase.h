// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ZmeykaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API AZmeykaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
