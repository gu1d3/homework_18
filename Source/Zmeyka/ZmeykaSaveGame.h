// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ZmeykaSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class ZMEYKA_API UZmeykaSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UZmeykaSaveGame();

	UPROPERTY(VisibleAnywhere, Category = "Player Info")
	int RecordSize;



	
};
