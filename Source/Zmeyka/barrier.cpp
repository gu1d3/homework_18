// Fill out your copyright notice in the Description page of Project Settings.


#include "barrier.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "SnakeBase.h"
#include "SpawnBox.h"

// Sets default values
Abarrier::Abarrier()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Abarrier::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Abarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Abarrier::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteSnakeElement();
			Destroy();
		}
		
	}
}